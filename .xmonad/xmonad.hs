-- Imports --   
import System.Exit -- Used to evit the WM properly
import XMonad -- Needed for base setup
import XMonad.Config.Desktop -- Alternate default config better for desktops
import XMonad.Hooks.DynamicLog -- XMobar setup
import XMonad.Hooks.ManageHelpers -- For extra ManageHooks parametres
import XMonad.Layout.NoBorders (smartBorders) -- Used so that focused window has no border when fullscreen
import XMonad.Util.EZConfig -- 
import XMonad.Util.SpawnOnce -- For startup hook

-- Definitions --

myLayoutHook = smartBorders $ layoutHook def -- Fives borders on fullscreen windows

-- Stuff here runs at WM startup --
myStartupHook = do
        spawnOnce "xsetroot -cursor_name left_ptr" -- Sets standard cursor as in some cases it messe up without this
        spawnOnce "xmobar $HOME/.config/xmobar/xmobarrc" -- Spawns status bar
        spawnOnce "trayer --widthtype request --height 15 --distance 2 --align right --SetPartialStrut true --transparent true --alpha 0 --tint 0x00000000" -- Spawns app tray at bottom right of status bar

-- XMobar config --
myPP = xmobarPP { ppCurrent =
          xmobarColor "Yellow" "" . wrap "<" ">"
        , ppHidden = wrap "[" "]"
        , ppHiddenNoWindows = wrap "" ""
        , ppUrgent = xmobarColor "" "Red"
        }

-- Workspace names on XMobar --
myWorkspaces =
        [ "browser"
        , "chat"
        , "game"
        , "torrents"
        ] ++ map show [ 5 .. 9 ]

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Removes unwanted default keybinds --
myRemovedKeys =
        [ ("M-S-<Return>")
        , ("M-p")
        , ("M-S-p")
        , ("M-S-q")
        ]


-- Adds in custom keybinds --
myAdditionalKeys =
        [ ("M-<Return>", spawn myTerminal) -- Spawns URxvt
        , ("M-d", spawn "dmenu_run -b") -- Runs DMenu
        , ("M-M1-q", io (exitWith ExitSuccess)) -- Exits WM
        ]

myTerminal = "urxvt"

-- On app spawn, moves or changes their behaviour in relation to the WM --
myManageHook = composeAll
        [ className =? "discord" --> doShift "chat"
        , className =? "firefox" --> doShift "browser"
        , className =? "Gimp" --> doFloat
        , className =? "Steam" --> doShift "game"
        , className =? "URxvt" --> doCenterFloat
        ]

myBar = "xmobar"

-- Sets the config to custom changes that are then run in the main function --
myConfig = desktopConfig
        { terminal = myTerminal -- Sets terminal
        , focusFollowsMouse = myFocusFollowsMouse -- Sets mouse follow rule
        , modMask = mod4Mask -- Sets WM main key
        , borderWidth = 1 -- Sets border width
        , manageHook = myManageHook -- Sets Window spawn hooks
        , workspaces = myWorkspaces -- Changes name of workspaces
        , startupHook = myStartupHook -- Spawns programs on startup
        , layoutHook = myLayoutHook -- Removes fullscreen borders
        }
        `removeKeysP` myRemovedKeys -- Removes unwanted keybinds
        `additionalKeysP` myAdditionalKeys -- Binds new custom keybinds

-- Main function --
main = xmonad =<< statusBar myBar myPP def myConfig
