# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix # Include results of the hardware scan.
    ];

  # Set up Nix Flakes support.
  nix = {
    extraOptions = "experimental-features = nix-command flakes";
    package = pkgs.nixFlakes;
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.

  time.timeZone = "US/Central"; # Set time zone.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s3.useDHCP = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # DWM Overlay (for patches)
  nixpkgs.overlays = [
    (self: super: {
      dwm = super.dwm.overrideAttrs (oldAttrs: rec {
        patches = [
          ./config/DWM/config.diff
          ./config/DWM/actual-fullscreen.diff
          ./config/DWM/always-center.diff
        ];
      });
      st = super.st.overrideAttrs (oldAttrs: rec {
        patches = [
          ./config/ST/config.diff
          ./config/ST/blinking-cursor.diff
          ./config/ST/clipboard.diff
          ./config/ST/default-font-size.diff
          ./config/ST/theme-nord.diff
          ./config/ST/true-bold.diff
        ];
      });
    })
  ];

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    displayManager = {
      defaultSession = "none+dwm";
      autoLogin = {
        enable = true;
        user = "nicholas";
      };
    };
    desktopManager.xterm.enable = true;
    ##windowManager.i3 = {
    ##  enable = true;
    ##  extraPackages = with pkgs; [
    ##    dmenu
    ##    i3status
    ##    i3lock
    ##    i3blocks
    ##    feh
    ##    alacritty
    ##  ];
    ##};
    windowManager.dwm.enable = true;
    layout = "us,halmak";
    xkbOptions = "grp:caps_toggle";
    extraLayouts.halmak = {
      description = "US Halmak Layout";
      languages = [ "eng" ];
      symbolsFile = ./config/halmak;
    };
    videoDrivers = [ "nvidia" ]; # Enable proprietary video drivers.
  };

  programs.fish.enable = true; # Enable Fish shell.

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable 32bit opengl
  hardware.opengl = {
    enable = true;
    driSupport32Bit = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.nicholas = {
    isNormalUser = true;
    shell = pkgs.fish; # Set default shell to Fish
    createHome = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  };

  # Set up DOAS as a SUDO replacement.
  security.doas = {
    enable = true;
    extraRules = [
      { groups = [ "wheel" ];
        noPass = false;
        keepEnv = true;
      }
    ];
    wheelNeedsPassword = true;
  };

  # Enable gnupg
  programs.gnupg.agent = {
    enable = true;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget
    vim
    dmenu
    feh
    st
    fantasque-sans-mono
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}

