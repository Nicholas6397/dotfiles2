#!/bin/sh

sudo nix-collect-garbage -d
sudo nix-store --optimise

nix flake update

pushd ~/.dotfiles
sudo nixos-rebuild switch --flake .#

nix build .#homeManagerConfigurations.nicholas.activationPackage
./result/activate
popd
