{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "nicholas";
  home.homeDirectory = "/home/nicholas";

  home.packages = with pkgs; [
    git
    git-crypt
    brave
    neofetch
    xdg-user-dirs
  ];

  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    plugins = with pkgs.vimPlugins; [
      vim-nix
    ];
  };

  home.file = {
    ".config/i3/config" = {
      source = ./config/i3.conf;
      recursive = true;
    };

    ".config/i3status/config" = {
      source = ./config/i3status.conf;
      recursive = true;
    };

    ".config/neofetch/config.conf" = {
      source = ./config/neofetch.conf;
      recursive = true;
    };
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";
}
