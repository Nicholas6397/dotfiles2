{
  description = "Hanakiri's Configuration Flake";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-20.09";
    home-manager.url = "github:nix-community/home-manager/release-20.09";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { nixpkgs, home-manager, ... }:
  let
    system = "x86_64-linux";

    pkgs = import nixpkgs {
      inherit system;
      config = { allowUnfree = true; };
    };

    lib = nixpkgs.lib;

  in {
    homeManagerConfigurations = {
      nicholas = home-manager.lib.homeManagerConfiguration {
        inherit system pkgs;
        username = "nicholas";
        homeDirectory = "/home/nicholas";
        configuration = {
          imports = [ ./users/nicholas/home.nix ];
        };
      };
    };

    nixosConfigurations = {
      nixos = lib.nixosSystem {
        inherit system;

        modules = [ ./system/configuration.nix ];
      };
    };
  };
}
