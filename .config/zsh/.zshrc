# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' matcher-list '' '' '' ''
zstyle :compinstall filename '/home/nicholas/.config/zsh/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
autoload -U colors
colors
:
HISTFILE=$HOME/.cache/zsh/.histfile
echo "" > $HISTFILE
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory beep notify
unsetopt autocd extendedglob nomatch
bindkey -e
# End of lines configured by zsh-newuser-install

## Custom config
alias sudo="sudo "
alias doas="doas "
alias ls='ls --color=auto -F'
alias ll="ls -lhAF"
alias lock="i3lock -e -i /home/nicholas/Pictures/Wallpapers/1920x1080/Anime_Angel.png &! exit"
alias vi="nvim"
alias vim="nvim"
alias emacs="emacs -nw"
alias angband="angband -mgcu -- aB && exit"
alias shutdown="shutdown -P now"
alias grep="grep --color=auto"

# Arch wiki search
alias wiki="wiki-search-html"
alias wikit="wiki-search"

# Pacman aliases
alias pkg="pacmatic"
alias pacman="pacmatic"
alias paclog="pacolog"

alias fortune="fortune | cowsay | lolcat"
alias sl="sl -Fl | lolcat"
alias clock="tty-clock -Scd 60"

#PROMPT='%B%F{magenta}[%F{red}%n%F{yellow}@%F{cyan}%1~%F{magenta}]%b%f%(!.λ.%%) '
PROMPT='%B%F{magenta}[%F{red}%n%F{yellow}@%F{cyan}%1~%F{magenta}]%b%F{green}%(!.%BΩ%b.%Bλ%b)%f '
RPROMPT='[%F{yellow}%D{%d %b. %R}%f]'

export EDITOR="nvim"
export SUDO_EDITOR="nvim"
export VISUAL="nvim"
export SUDO_VISUAL="nvim"
export wiki_lang="en"
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup

#if [[ $DISPLAY ]]; then
#	[[ $- != *i* ]] && return
#	[[ -z "$TMUX" ]] && exec tmux
#fi

if [[ $DISPLAY ]]; then
    [[ $- != *i* ]] && return
    if [[ $(ps --no-header --pid=$PPID --format=cmd) != "fish" ]]; then
	exec fish
    fi
fi
