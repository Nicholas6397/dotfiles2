filetype indent on
filetype plugin on
syntax on
set relativenumber
set shiftwidth=8
autocmd BufRead *.hs set et
