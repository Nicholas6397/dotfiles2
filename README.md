# dotfiles

My configuration files for my home Linux desktop (Arch is used as my daily driver but I regularly experiment with NixOS and may one day fully switch to it)

![](https://vignette.wikia.nocookie.net/love-live/images/d/df/NicoTrasparente.png/revision/latest?cb=20160531215349&path-prefix=es)
